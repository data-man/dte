#ifndef MODE_H
#define MODE_H

#include "editor.h"
#include "terminal/key.h"

void handle_input(EditorState *e, KeyCode key);

#endif
